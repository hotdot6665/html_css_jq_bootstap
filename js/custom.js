$(document).ready(function () {
    $("#myCarousel").carousel({
        interval: false
    });
});

$(window).scroll(function () {
    var height = $(window).scrollTop();
    if (height > 50) {
        $('header').css('position', 'fixed').css('background-color', '#16151b');
    } else {
        $('header').css('position', 'relative').css('background-color', 'transparent');
    }
});